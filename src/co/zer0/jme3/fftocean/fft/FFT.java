package co.zer0.jme3.fftocean.fft;

import java.util.ArrayList;

/**
 *
 * @author kalockhart
 */
public class FFT {

    private final int n;
    private final int p;
    private ArrayList<Double> real;
    private ArrayList<Double> imag;

    public FFT(int n, ArrayList<Double> real, ArrayList<Double> imag) {
        this.n = n;
        this.p = (Integer.SIZE - 1) - Integer.numberOfLeadingZeros(n); // log2(n)
        this.real = real;
        this.imag = imag;
    }

    public void direct() {
        this.sort();
        this.radix_direct();
    }

    public void reverse() {
        this.sort();
        this.radix_reverse();
    }

    /**
     * Direct FFT Transform. The algorithm computes the spectrum for the time
     * domain signal in the real and imag arrayLists, and stores the result in
     * these same vectors.
     */
    private void radix_direct() {
        int nCopy = this.n;
        ArrayList<Double> realCopy = new ArrayList<>();
        ArrayList<Double> imagCopy = new ArrayList<>();

        for (int z = 0; z < n; z++) {
            realCopy.add(0d);
            imagCopy.add(0d);
        }

        // Repeat the process p times
        for (int i = 0; i < p; i++) {
            // Compute n/2 values and use them twice
            for (int j = 0; j < nCopy / 2; j++) {
                for (int k = 0; k < Math.pow(2, i); k++) {
                    final int index1 = (int) (k + j * Math.pow(2, i + 1));
                    final int index2 = (int) (index1 * Math.pow(2, i));
                    final double var = (double) (-(2 * Math.PI) / Math.pow(2, i + 1)) * index1;
                    final double vCos = Math.cos(var);
                    final double vSin = Math.sin(var);
                    final double imag2 = imag.get(index2);
                    final double real2 = real.get(index2);
                    final double real1 = real.get(index1);
                    final double imag1 = real.get(index1);
                    realCopy.add(index1, real1 + vCos * real2 - vSin * imag2);
                    realCopy.add(index2, real1 - vCos * real2 + vSin * imag2);
                    imagCopy.add(index1, imag1 + vCos * imag2 + vSin * real2);
                    imagCopy.add(index2, imag1 - vCos * imag2 - vSin * real2);

                }

            }

            ArrayList<Double> tmp;

            // swap realcopy and real
            tmp = realCopy;
            realCopy = real;
            real = tmp;

            // swap imagcopy and imag
            tmp = imagCopy;
            imagCopy = imag;
            imag = tmp;
            nCopy /= 2;
        }
    }

    /**
     * Reverse FFT Transform. The algorithm computes the spectrum for the time
     * domain signal in the real and imag arrayLists, and stores the result in
     * these same vectors.
     */
    private void radix_reverse() {
        int nCopy = this.n;
        ArrayList<Double> realCopy = new ArrayList<>();
        ArrayList<Double> imagCopy = new ArrayList<>();

        for (int z = 0; z < n; z++) {
            realCopy.add(0d);
            imagCopy.add(0d);
        }

        // Repeat the process p times
        for (int i = 0; i < p; i++) {
            // Compute n/2 values and use them twice
            for (int j = 0; j < nCopy / 2; j++) {
                for (int k = 0; k < Math.pow(2, i); k++) {
                    final int index1 = (int) (k + j * Math.pow(2, i + 1));
                    final int index2 = (int) (index1 * Math.pow(2, i));
                    final double var = (double) ((2 * Math.PI) / Math.pow(2, i + 1)) * index1;
                    final double vCos = Math.cos(var);
                    final double vSin = Math.sin(var);
                    final double imag2 = imag.get(index2);
                    final double real2 = real.get(index2);
                    final double real1 = real.get(index1);
                    final double imag1 = real.get(index1);
                    realCopy.add(index1, real1 + vCos * real2 - vSin * imag2);
                    realCopy.add(index2, real1 - vCos * real2 + vSin * imag2);
                    imagCopy.add(index1, imag1 + vCos * imag2 + vSin * real2);
                    imagCopy.add(index2, imag1 - vCos * imag2 - vSin * real2);

                }

            }

            ArrayList<Double> tmp;

            // swap realcopy and real
            tmp = realCopy;
            realCopy = real;
            real = tmp;

            // swap imagcopy and imag
            tmp = imagCopy;
            imagCopy = imag;
            imag = tmp;
            nCopy /= 2;
        }
    }

    /**
     * Sort. Sorts the data sor that the radix algorithm can be applied. In the
     * first step, the sorting puts all the evenly indexed values first and the
     * oddly indexed values second in the array. For the following steps, the
     * same process is applied to arrays of lenth l/2 with l the length of the
     * previous array. At the end all the values are interleaved.
     */
    private void sort() {
        int nCopy = n;

        for (int i = 0; i < p - 1; i++) {
            ArrayList<Double> sortedR = new ArrayList<>();
            ArrayList<Double> sortedI = new ArrayList<>();
            sortedR.ensureCapacity(n);
            sortedI.ensureCapacity(n);

            ArrayList<Double> vectRp = new ArrayList<>();
            ArrayList<Double> vectIp = new ArrayList<>();
            ArrayList<Double> vectRi = new ArrayList<>();
            ArrayList<Double> vectIi = new ArrayList<>();
            for (int init = 0; init < nCopy / 2; init++) {
                vectRp.add(0d);
                vectIp.add(0d);
                vectRi.add(0d);
                vectIi.add(0d);
            }
            int idxR = 0;
            int idxI = 0;
            
            for (int j = 0; j < n / nCopy; j++) {
                // reorganze sub array
                for (int k = 0; k < nCopy / 2; k++) {
                    final int index = (int) 2 * k + j * nCopy;
                    vectRp.add(k, real.get(index));
                    vectIp.add(k, imag.get(index));
                    vectRi.add(k, real.get(index + 1));
                    vectIi.add(k, imag.get(index + 1));
                }
                for (double d : vectRp) {
                    sortedR.add(d);
                }
                
                for (double d : vectIp) {
                    sortedI.add(d);
                }
                
                for (double d : vectRi) {
                    sortedR.add(d);
                }
                
                for (double d : vectIi) {
                    sortedI.add(d);
                }                

            }

            ArrayList<Double> tmp;

            // swap real and sortedR
            tmp = sortedR;
            sortedR = real;
            real = tmp;

            // swap imag and sortedI
            tmp = sortedI;
            sortedI = imag;
            imag = tmp;
            nCopy /= 2;
        }

    }
}

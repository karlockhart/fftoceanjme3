package co.zer0.jme3.fftocean.ocean;

/**
 *
 * @author kalockhart
 */
public class Phillipps {
    /* sea state */
    protected State currentState;
    /* equals i-nx/2 in a series of calls to the function */
    private int x;
    /* goes in the range [ -ny/2 : ny/2 ] in a series of calls to the function */
    private int y;
    
    Phillipps(State state) {
       this.currentState = state;
    }
    
    /*
     * @void   
     */
    void init(int i) {
        this.x = i - currentState.nx/2;
        this.y = - currentState.ny/2;
    }
    
    /*
     * @double  
     */
    double phillipps() {
        final double g = 9.81;
        final double kx = (2 * Math.PI*this.x)/currentState.lx;
        final double ky = (2 * Math.PI*this.y)/currentState.ly;
        final double kSq = kx * kx + ky * ky;
        final double lSq = Math.pow((currentState.windSpeed * currentState.windSpeed)/g, 2);
        y++;
        
        if (kSq == 0) {
            return 0;
        } else {
            double var;
            var = currentState.A * Math.exp((-1/(kSq *lSq)));
            var *= Math.exp(-kSq*Math.pow(currentState.minWaveSize,2));
            var *= Math.pow((kx * kx)/kSq, currentState.windAlignment);
            return var;
        }
    }
}

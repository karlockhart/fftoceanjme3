/*

*/
package co.zer0.jme3.fftocean.ocean;

/**
 *
 * @author kalockhart
 */
public class State {
    
    /* actual width of the scene */
    protected double lx;
    /* actual height of the scene */
    protected double ly;
    /* num of x points, must be a power of 2 */
    protected int nx;
    /* num of y points, must be a power of 2 */
    protected int ny;
    /* wind speed */
    protected double windSpeed;
    /* waves in this alignment are enhanced */
    protected double windAlignment;
    /* numerical constant to adjust the waves height */
    protected double A;
    /* motionFactor */
    protected double motionFactor;
    /* */
    protected double minWaveSize;
    
    State (
            double lx,
            double ly,
            int nx,
            int ny,
            double windSpeed,
            double windAlignment,
            double A,
            double motionFactor,
            double minWaveSize
    ) {
        this.lx = lx;
        this.ly = ly;
        this.nx = nx;
        this.ny = ny;
        this.windSpeed = windSpeed;
        this.windAlignment = windAlignment;
        this.A = A;
        this.motionFactor = motionFactor;
        this.minWaveSize = minWaveSize;
    }
}

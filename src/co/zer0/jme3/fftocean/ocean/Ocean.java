/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.zer0.jme3.fftocean.ocean;

import co.zer0.jme3.fftocean.fft.FFT;
import java.util.ArrayList;

/**
 *
 * @author kalockhart
 */
public class Ocean {
    
    // actual width 
    private final double lx;
    
    // actual height
    private final double ly;
    
    // number of x points - must be a power of 2
    private final int nx;
    
    // number of y points - must be a power of 2
    private final int ny;
    
    private final double motionFactor;
    
    // initial wave height field (spectrum) - real part
    private ArrayList<ArrayList<Double>> heightZeroR;
    // initial wave height field (spectrum) - imaginary part
    private ArrayList<ArrayList<Double>> heightZeroI;
    
    // frequency domain - real part
    private ArrayList<ArrayList<Double>> HR;
    // frequency domain - imaginary part
    private ArrayList<ArrayList<Double>> HI;
    
    // time domain - real part
    private ArrayList<ArrayList<Double>> hr;
    // time domain - imaginary part
    private ArrayList<ArrayList<Double>> hi;
    
    private ArrayList<FFT> fftx;
    private ArrayList<FFT> ffty;
    
    
    Ocean(double lx, double ly, int nx, int ny, double motionFactor) {
        this.lx = lx;
        this.ly = ly;
        this.nx = nx;
        this.ny = ny;
        this.motionFactor = motionFactor;
     
        // initialize array lists, doubles assumed value initialed to 0
        heightZeroI = new ArrayList<>();
        heightZeroR = new ArrayList<>();
        HR = new ArrayList<>();
        HI = new ArrayList<>();
        hr = new ArrayList<>();
        hi = new ArrayList<>();
        for (int inx = 0; inx <= nx; inx++){
            ArrayList<Double> iheightZeroI = new ArrayList<>();
            ArrayList<Double> iheightZeroR = new ArrayList<>();
            ArrayList<Double> iHR = new ArrayList<>();
            ArrayList<Double> iHI = new ArrayList<>();
            ArrayList<Double> ihr = new ArrayList<>();
            ArrayList<Double> ihi = new ArrayList<>();
            for (int iny = 0; iny <= ny; iny ++) {
                iheightZeroI.add(0d);
                iheightZeroR.add(0d);
                iHR.add(0d);
                iHI.add(0d);
                ihr.add(0d);
                ihi.add(0d);
            }
            heightZeroI.add(iheightZeroI);
            heightZeroR.add(iheightZeroR);
            HR.add(iHR);
            HI.add(iHI);
            hr.add(ihr);
            hi.add(ihi);            
        }
        
        fftx = new ArrayList<>();
        ffty = new ArrayList<>();
        fftx.ensureCapacity(nx);
        ffty.ensureCapacity(ny);
        
        for (int i = 0; i <nx; i++) {
            ffty.add(new FFT(ny, HR.get(i), HI.get(i)));
        }
        
        for (int i = 0; i < ny; i++) {
            fftx.add(new FFT(nx, hr.get(i), hi.get(i)));
        }
    }
}

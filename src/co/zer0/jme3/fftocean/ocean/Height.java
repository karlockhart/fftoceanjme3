/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.zer0.jme3.fftocean.ocean;

import java.util.ArrayList;

/**
 *
 * @author kalockhart
 */
public class Height {
    int nx;
    int ny;
    ArrayList<ArrayList<Double>> phillips;
    int x;
    int y;
    
    Height(int nx, int ny) {
        this.nx = nx;
        this.ny = ny;
    }
    
    /*
     * @void
     */
    void init(int i) {
        this.x = i - nx/2;
        this.y = -ny/2;
    }
    
    double height() {
        this.y++;
        return Math.sqrt(phillips.get(x+(nx/2)).get(y + (ny/2))/2) * this.gaussian();
    }

    
    void generatePhillips(Phillipps p){
        for(int i = 0; i <= nx; i++) {
            p.init(i);
            phillips.add(new ArrayList<>());
            for (int j = 0; j <= ny; j++) {
                phillips.get(i).add(p.phillipps());
            }
        }
    }
    
    double gaussian() {
        double var1;
        double var2;
        double s;
        
        do {
            var1 = (Math.random() * 201 - 100)/100d;
            var2 = (Math.random() * 201 - 100)/100d;
            s = var1*var1 + var2*var2;
        } while(s>=1 || s==0);
        return s;
    }   
}
